<!DOCTYPE html>
<html>
    <head>
        @include('header')
    </head>
    <body>
        <!--<h2>Login Form</h2>-->
        @include('static/errors')
        @include('static/loggedDashboard')
        @include('static/success')
        <div class="tableForm">
            <div class="dbimgcontainer" ></div>
            <div class="container">
                <div class="infobox">Witaj użytkowniku <b>{{ Auth::user()->getAttribute('login') }}</b><br/> Jesteś zalogowany jako <b>{{ $roleName }}</b></br>Wybrana tabela: <b>{{ $tableName }}</b><br/>tryb przeglądania</div>                 
               @if ($tableName == 'Role')
                    @include('_rolesDisplay_')
               @else
                    @include('_defaultDisplay_')
               @endif
                <div class="buttonsContainer">
                <a href="./"><div class="returnButton">powrót</div></a>  
                </div>
            </div>            
        </div>
    </body>
</html>




