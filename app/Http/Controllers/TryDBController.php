<?php namespace App\Http\Controllers;

use DB;
use Auth;
use Session;
use App\Role;
use App\User;
use App\DbTables;
use App\Permissions;

class TryDBController extends Controller{

    public function __construct(){
        //$this->middleware('auth');
    }
    
    public function index()
    {
        //phpinfo();
//            try {
//                DB::connection()->getPdo();
//                return "OK";
//            } catch (\Exception $e) {
//                return "FAIL";
//                die("Could not connect to the database.  Please check your configuration.");
//            }

        try {

            //$DBH = new PDO("sqlsrv:Server=XP\SQLEXPRESS;Database=bskDB;ConnectionPooling=0", "bskDB", "1234");
            //$this->db = new PDO ("dblib:host=$this->hostname:$this->port;dbname=$this->dbname", "$this->username", "$this->pwd");
            DB::connection()->getPdo();
            
            $table = DB::table('Uzytkownicy')->get(); // cala tablela
            $table = DB::table('Uzytkownicy')->pluck("Login", "CzyZalogowany"); // nie dziakla
            $table = DB::table('Uzytkownicy')->first(); // pierwszy caly wiesz

            //var_dump(Auth::user());
            //var_dump(Auth::id());
            //echo var_dump($table);
            echo "DZIALA";

            // wydobycie uprawnien
//            $a = Auth::user()->getPermissions('Samochody');
//            var_dump($a->isReadPermission());
//            var_dump($a->isDeletePermission());
//            var_dump($a->isCreatePermission());
//            var_dump($a->isUpdatePermission());
//            
            // wydobycie tabel do ktorych uzytkownik ma dostep
            //var_dump(DbTables::getTablesForUser(Auth::user()));
            
            var_dump(\App\User::find(1)->id);
            echo '<hr>';
            $rola = Role::firstByAttributes(['NazwaRoli' => 'a']);
            $rola = Session::get('roleName');
            var_dump($rola);
            echo '<hr>';
            $sessionRoleName = Session::get('roleName');
            //$role = Role::getRoleFromName($sessionRoleName);
            $userRoles = Auth::user()->roles->lists('IDRola', 'NazwaRoli');
            
            var_dump($userRoles);
            
            var_dump(Auth::user()->role);
            var_dump(Auth::user()->role->getKey());
            
            {
                        $sessionRoleName = "a";
        
                        // if terminal has't got any role (in session)
                       if (is_null($sessionRoleName)) 
                           return redirect()->guest($this::ROLE_REDIRECT);

                       // if user isn't logged as any role (in database)
                       if (is_null(Auth::user()->role))
                           return redirect()->guest($this::ROLE_REDIRECT);

                       // fetch dictionary 'NazwaRoli' => 'IDRola' for this user (based on his roles)
                       //$userRoles = Auth::user()->roles->lists('IDRola', 'NazwaRoli');

                       // check if user has this role
                       $role = Auth::user()->roles->where('NazwaRoli', $sessionRoleName)->first();
                       if (is_null($role))
                           return redirect()->guest($this::ROLE_REDIRECT);

                       // check if user is logged as this role
                       if (Auth::user()->role->getKey() !== $role->getKey())
                           return redirect()->guest($this::ROLE_REDIRECT);
                       
                       echo "OK";
            }
            
        } catch (PDOException $e) {
            echo "NIE DZIALA";
            echo $e->getMessage();
        }

//            if (DB::connection()->getDatabaseName())
//            {
//               return 'Connected to the DB: ' . DB::connection()->getDatabaseName();
//            }
    }
}
